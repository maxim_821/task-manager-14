package ru.ivanov.tm.service;

import ru.ivanov.tm.api.IProjectRepository;
import ru.ivanov.tm.api.IProjectTaskService;
import ru.ivanov.tm.api.ITaskRepository;
import ru.ivanov.tm.model.Project;
import ru.ivanov.tm.model.Task;

import java.util.HashSet;

public class ProjectTaskService implements IProjectTaskService {

    private ITaskRepository taskRepository;

    private IProjectRepository projectRepository;

    public ProjectTaskService(ITaskRepository taskRepository, IProjectRepository projectRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @Override
    public HashSet<Task> findAllByProjectId(final String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        return taskRepository.findAllByProjectId(projectId);
    }

    @Override
    public Task bindTaskByProject(final String id, final String projectId) {
        if (id == null || id.isEmpty()) return null;
        if (projectId == null || projectId.isEmpty()) return null;
        final Task task = taskRepository.bindTaskByProject(id, projectId);
        if (task == null) return null;
        return task;
    }

    @Override
    public Task unbindTaskFromProject(final String id) {
        if (id == null || id.isEmpty()) return null;
        final Task task = taskRepository.unbindTaskFromProject(id);
        if (task == null) return null;
        return task;
    }

    @Override
    public Project removeProjectById(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        taskRepository.removeAllByProjectId(projectId);
        return projectRepository.removeOneById(projectId);
    }
}
