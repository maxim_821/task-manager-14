package ru.ivanov.tm.enumerated;

import ru.ivanov.tm.comparator.ComparatorByCreated;
import ru.ivanov.tm.comparator.ComparatorByDateStart;
import ru.ivanov.tm.comparator.ComparatorByName;
import ru.ivanov.tm.comparator.ComparatorByStatus;

import java.util.Comparator;

public enum Sort {

    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    NAME("Sort by name", ComparatorByName.getInstance()),
    DATE_START("Sort by date start", ComparatorByDateStart.getInstance()),
    STATUS("Sort by status", ComparatorByStatus.getInstance());

    private final String displayName;

    private final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }

}
