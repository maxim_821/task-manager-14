package ru.ivanov.tm.api;

import java.util.Date;

public interface IHasCreated {

    Date getCreated();

    void setCreated(Date date);

}
