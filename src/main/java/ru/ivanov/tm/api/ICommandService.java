package ru.ivanov.tm.api;

import ru.ivanov.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();
}
