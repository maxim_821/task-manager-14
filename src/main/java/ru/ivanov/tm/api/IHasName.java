package ru.ivanov.tm.api;

public interface IHasName {

    String getName();

    void setName(String name);

}
