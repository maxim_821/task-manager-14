package ru.ivanov.tm.api;

public interface ICommandController {

    void exit();

    void displayVersion();

    void displayAbout();

    void showCommands();

    void showArguments();

    void displayHelp();

    void showSystemInfo();

}
