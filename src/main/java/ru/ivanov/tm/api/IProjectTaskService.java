package ru.ivanov.tm.api;

import ru.ivanov.tm.model.Project;
import ru.ivanov.tm.model.Task;

import java.util.HashSet;

public interface IProjectTaskService {

    HashSet<Task> findAllByProjectId(String projectId);

    Task bindTaskByProject(String id, String projectId);

    Task unbindTaskFromProject(String projectId);

    Project removeProjectById(String projectId);

}
