package ru.ivanov.tm.api;

import ru.ivanov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
