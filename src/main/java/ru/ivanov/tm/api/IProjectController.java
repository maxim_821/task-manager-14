package ru.ivanov.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

    void showProjectByIndex();

    void showProjectByName();

    void showProjectById();

    void removeProjectByIndex();

    void removeProjectByName();

    void updateProjectByIndex();

    void updateProjectById();

    void startProjectById();

    void startProjectByIndex();

    void startProjectByName();

    void finishProjectById();

    void finishProjectByIndex();

    void finishProjectByName();

    void removeProjectById();

}
