package ru.ivanov.tm.api;

import ru.ivanov.tm.model.Task;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    HashSet<Task> findAllByProjectId(String projectId);

    List<Task> removeAllByProjectId(String projectId);

    Task bindTaskByProject(String id, String projectId);

    Task unbindTaskFromProject(String projectId);

    void add(Task task);

    void remove(Task task);

    void clear();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task findOneByName(String name);

    Task removeOneById(String id);

    Task removeOneByIndex(Integer index);

    Task removeOneByName(String name);

}
