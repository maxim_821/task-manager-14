package ru.ivanov.tm.api;

import ru.ivanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
