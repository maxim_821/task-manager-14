package ru.ivanov.tm.bootstrap;

import ru.ivanov.tm.api.*;
import ru.ivanov.tm.constant.ArgumentConst;
import ru.ivanov.tm.constant.TerminalConst;
import ru.ivanov.tm.controller.CommandController;
import ru.ivanov.tm.controller.ProjectController;
import ru.ivanov.tm.controller.TaskController;
import ru.ivanov.tm.repository.CommandRepository;
import ru.ivanov.tm.repository.ProjectRepository;
import ru.ivanov.tm.repository.TaskRepository;
import ru.ivanov.tm.service.CommandService;
import ru.ivanov.tm.service.ProjectService;
import ru.ivanov.tm.service.ProjectTaskService;
import ru.ivanov.tm.service.TaskService;
import ru.ivanov.tm.util.TerminalUtil;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    private final ITaskController taskController = new TaskController(taskService, projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    public void run(final String... args) {
        System.out.println("WELCOME TO TASK MANAGER");
        if (parseArgs(args)) System.exit(0);
        while (true) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    public void parseArg(final String arg) {
        if (arg == null) return;
        switch (arg) {
            case ArgumentConst.ARG_ABOUT:
                commandController.displayAbout();
                break;
            case ArgumentConst.ARG_HELP:
                commandController.displayHelp();
                break;
            case ArgumentConst.ARG_VERSION:
                commandController.displayVersion();
                break;
            case ArgumentConst.ARG_INFO:
                commandController.showSystemInfo();
                break;
            default:
                showIncorrectArgument();
        }
    }

    public void parseCommand(final String command) {
        if (command == null) return;
        switch (command) {
            case TerminalConst.CMD_ABOUT:
                commandController.displayAbout();
                break;
            case TerminalConst.CMD_HELP:
                commandController.displayHelp();
                break;
            case TerminalConst.CMD_VERSION:
                commandController.displayVersion();
                break;
            case TerminalConst.CMD_INFO:
                commandController.showSystemInfo();
                break;
            case TerminalConst.CMD_COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConst.CMD_ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConst.CMD_EXIT:
                commandController.exit();
                break;
            case TerminalConst.CMD_TASK_LIST:
                taskController.showList();
                break;
            case TerminalConst.CMD_TASK_CREATE:
                taskController.create();
                break;
            case TerminalConst.CMD_TASK_CLEAR:
                taskController.clear();
                break;
            case TerminalConst.CMD_PROJECT_LIST:
                projectController.showList();
                break;
            case TerminalConst.CMD_PROJECT_CREATE:
                projectController.create();
                break;
            case TerminalConst.CMD_PROJECT_CLEAR:
                projectController.clear();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_REMOVE_BY_NAME:
                taskController.removeTaskByName();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_VIEW_BY_NAME:
                taskController.showTaskByName();
                break;
            case TerminalConst.CMD_TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConst.CMD_TASK_START_BY_NAME:
                taskController.startTaskByName();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_ID:
                taskController.finishTaskById();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_INDEX:
                taskController.finishTaskByIndex();
                break;
            case TerminalConst.CMD_TASK_FINISH_BY_NAME:
                taskController.finishTaskByName();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_REMOVE_BY_NAME:
                projectController.removeProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_VIEW_BY_NAME:
                projectController.showProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConst.CMD_PROJECT_START_BY_NAME:
                projectController.startProjectByName();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_ID:
                projectController.finishProjectById();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_INDEX:
                projectController.finishProjectByIndex();
                break;
            case TerminalConst.CMD_PROJECT_FINISH_BY_NAME:
                projectController.finishProjectByName();
                break;
            case TerminalConst.CMD_TASK_FIND_BY_PROJECT:
                taskController.findAllByProjectId();
                break;
            case TerminalConst.CMD_TASK_BIND_BY_PROJECT:
                taskController.bindTaskByProject();
                break;
            case TerminalConst.CMD_TASK_UNBIND_BY_PROJECT:
                taskController.unbindTaskFromProject();
                break;
            case TerminalConst.CMD_PROJECT_AND_TASK_REMOVE:
                projectController.removeProjectById();
                break;
            default:
                showIncorrectCommand();
        }
    }

    public boolean parseArgs(String[] args) {
        if (args == null || args.length == 0) return false;
        final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public static void showIncorrectCommand() {
        System.out.println("Error! Command not found...");
    }

    public static void showIncorrectArgument() {
        System.out.println("Error! Argument not found...");
    }

}
